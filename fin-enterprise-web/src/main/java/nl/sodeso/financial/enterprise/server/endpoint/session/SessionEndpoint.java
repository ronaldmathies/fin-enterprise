package nl.sodeso.financial.enterprise.server.endpoint.session;

import nl.sodeso.gwt.ui.client.controllers.session.domain.LoginResult;
import nl.sodeso.gwt.ui.client.controllers.session.domain.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

/**
 * @author Ronald Mathies
 */
@WebServlet(urlPatterns = {"*.session"})
public class SessionEndpoint extends nl.sodeso.gwt.ui.server.endpoint.session.SessionEndpoint {

    /**
     * {@inheritDoc}
     */
    @Override
    public LoginResult login(String username, String password) {
        try {
            getThreadLocalRequest().login(username, password);
            return new LoginResult(new User(username));
        } catch (ServletException e) {}

        return new LoginResult(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Void logout() {
        try {
            getThreadLocalRequest().logout();
        } catch (ServletException e) {}

        return null;
    }
}
