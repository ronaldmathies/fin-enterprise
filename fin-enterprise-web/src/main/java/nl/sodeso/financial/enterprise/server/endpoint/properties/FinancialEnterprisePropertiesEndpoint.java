package nl.sodeso.financial.enterprise.server.endpoint.properties;

import nl.sodeso.financial.enterprise.client.Constants;
import nl.sodeso.financial.enterprise.client.properties.FinancialEnterpriseClientAppProperties;
import nl.sodeso.financial.enterprise.server.listener.FinancialEnterpriseServerAppProperties;
import nl.sodeso.gwt.ui.server.endpoint.properties.AbstractPropertiesEndpoint;
import nl.sodeso.gwt.ui.server.util.ApplicationPropertiesContainer;

import javax.servlet.annotation.WebServlet;

/**
 * By default the application has an endpoint for properties consisting of the following settings:
 *
 * <ul>
 *  <li>name: Name of the application (used as the title)</li>
 *  <li>version: Version number of the application.</li>
 *  <li>devkitEnabled: Are the extra development tools available.</li>
 * </ul>
 *
 * If you would like to have more global application settings (not user specific), that don't
 * change during a session, then this is the place to add them. Just add the fields below
 * and fill them using the endpoint.
 *
 * @author Ronald Mathies
 */
@WebServlet(urlPatterns = {"*.financialenterprise-properties"})
public class FinancialEnterprisePropertiesEndpoint extends AbstractPropertiesEndpoint<FinancialEnterpriseClientAppProperties, FinancialEnterpriseServerAppProperties> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<FinancialEnterpriseClientAppProperties> getClientAppPropertiesClass() {
            return FinancialEnterpriseClientAppProperties.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fillApplicationProperties(FinancialEnterpriseServerAppProperties serverAppProperties, FinancialEnterpriseClientAppProperties clientAppProperties) {
        clientAppProperties.setName(Constants.MODULE_ID);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FinancialEnterpriseServerAppProperties getServerAppProperties() {
        return ApplicationPropertiesContainer.instance().getApplicationProperties(Constants.MODULE_ID);
    }

}

