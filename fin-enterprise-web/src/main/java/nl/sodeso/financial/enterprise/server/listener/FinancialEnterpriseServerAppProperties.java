package nl.sodeso.financial.enterprise.server.listener;

import nl.sodeso.gwt.ui.server.util.ServerAppProperties;

/**
 * @author Ronald Mathies
 */
public class FinancialEnterpriseServerAppProperties extends ServerAppProperties {

    public FinancialEnterpriseServerAppProperties(String configurationFile) {
        super(configurationFile);
    }
}
