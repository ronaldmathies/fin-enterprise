package nl.sodeso.financial.enterprise.client;

import com.google.gwt.core.client.GWT;
import nl.sodeso.financial.enterprise.client.properties.FinancialEnterpriseClientAppProperties;
import nl.sodeso.gwt.ui.client.application.AvailableModulesService;
import nl.sodeso.gwt.ui.client.application.GwtApplicationEntryPoint;
import nl.sodeso.gwt.ui.client.application.module.ModuleEntryPoint;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The application entry point is the main method of a GWT application, it should handle
 * the boodstrapping of the application.
 *
 * @author Ronald Mathies
 */
public class FinancialEnterpriseEntryPoint extends GwtApplicationEntryPoint<FinancialEnterpriseClientAppProperties> {

    private static Logger logger = Logger.getLogger("Application.Boot");

    /**
     * {@inheritDoc}
     */
    @Override
    public String getModuleId() {
        return Constants.MODULE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBeforeApplicationLoad() {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ModuleEntryPoint> modules(){
        return ((AvailableModulesService)GWT.create(AvailableModulesService.class)).availableModules();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onAfterApplicationLoad() {
        // Activate the initial module in this phase.
        logger.log(Level.INFO, "Activating initial module.");
        activateModule(nl.sodeso.financial.contacts.client.Constants.MODULE_ID);
    }
}
