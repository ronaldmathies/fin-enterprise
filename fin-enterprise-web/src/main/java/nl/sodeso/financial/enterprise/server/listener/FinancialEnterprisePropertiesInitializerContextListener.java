package nl.sodeso.financial.enterprise.server.listener;

import nl.sodeso.financial.enterprise.client.Constants;
import nl.sodeso.gwt.ui.server.listener.AbstractPropertiesInitializerContextListener;

import javax.servlet.annotation.WebListener;

/**
 * @author Ronald Mathies
 */
@WebListener
public class FinancialEnterprisePropertiesInitializerContextListener extends AbstractPropertiesInitializerContextListener {

    /**
     * {@inheritDoc}
     */
    @Override
    public Class getServerAppPropertiesClass() {
        return FinancialEnterpriseServerAppProperties.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getConfiguration() {
        return "fin-enterprise-configuration.properties";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getModuleId() {
        return Constants.MODULE_ID;
    }
}
